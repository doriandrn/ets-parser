module.exports = {
  verbose: true,
  bail: false,
  coverageReporters: [
    'html',
    'text'
  ],
  testEnvironment: 'node',
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json',
      diagnostics: false
    }
  },
  moduleFileExtensions: [
    'js',
    'jsx',
    'json',
    'd.ts',
    '.ets',
    'ts',
    'tsx'
  ],
  moduleNameMapper: {
    '@/(.*)$': '<rootDir>/$1',
    '~/(.*)$': '<rootDir>/src/$1',
    'f/(.*)$': '<rootDir>/__tests__/__fixtures__/',
  },
  modulePathIgnorePatterns: [
    "<rootDir>/dist/",
    "<rootDir>/tests/__fixtures__/",
  ],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
    '^.+\\.tsx?$': 'ts-jest'
  },
  preset: 'ts-jest',
  // transformIgnorePatterns: ['/node_modules/'],
  // testRegex: '(/__tests__/.*| (\\.| /)(test|spec))\\.(jsx?|tsx?)$'
  testRegex: null
}
