// const schemas = {}

type ParserOptions = {
  singular: string,
  plural ?: string
}

type ParsedSchema = {
  singular: string,
  parents: Taxonomy[],
  fields: {
    [k: string]: any
  },
  methods: {
    [k: string]: (context ?: any) => any
  }
  types: {
    [k: string]: string
  },
  interfaces: {
    [k: string]: string
  }
}

export default (input: string = '', opts : ParserOptions) => {
  let { singular, plural } = opts
  plural = plural || `${singular}s`

  const parsed: ParsedSchema = {
    singular,

    fields: {},
    methods: {},

    types: {},
    interfaces: {},
  }

  const { fields } = parsed

  let activeKey: string, activeKeyKeywords: string[] = []
  let primarySupplied: string, activeProp: string

  const s = String(input)
    .split('\n')
    .filter(line => line.length > 2) // broken lines
    .map(line => line
      .replace(/\t/g, '  ') // replace tabs with spaces
      .replace(/\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm, '$1') // remove comments, keep urls
    )

  s
    .map((line, i) => {
      if (s.length === i + 1) {
        // console.log('reachedLastLine')
      }
      if (line.indexOf('@parents') === 0) {
        const d = line.split(/ (.+)/)
        const parents: string[] = d[1]
          .split(',')
          .map(parent => parent.trim())

        Object.assign(parsed, { parents })
      } else {
        if (line.indexOf(' ') !== 0) {
          const kv = line.split(':')
          let key = kv[0].trim()
          const hasParanthesis = /\(([^)]+)\)/
          const matches = hasParanthesis.exec(key)

          if (matches && matches.length) {
            // methods[key] = matches[0]
          }
          else if (key.indexOf('@') === 0) {
            key = key.replace('@', '')
            if (!primarySupplied) {
              primarySupplied = key
            } else {
              throw new Error('Schema cannot have 2 primaries.')
            }
          }

          fields[key] = {}
          activeKey = key

          const value = kv[1] ? kv[1].trim() : undefined

          if (primarySupplied === key) {
            activeKeyKeywords.push('primary')
          }

          if (value && value.indexOf('!') >= 0 && value.indexOf('!') < 2) {
            activeKeyKeywords.push('index')
          }

          if (value && value.indexOf('?') >= 0 && value.indexOf('?') < 2) {
            activeKeyKeywords.push('required')
          }
          activeKeyKeywords.map(keyword => {
            fields[key][keyword] = true
          })
          activeKeyKeywords = []
        } else {
          // addProperty(activeKey, line)
          const data = line.trim().split(' ')
          fields[activeKey][data[0]] = data[1] ? data[1].trim() : {}
        }
      }
      // return line
    })

  return parsed
}
