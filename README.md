# ETS Parser

Parser for Extensible Typed Schemas

## How it works

### Keywords

| keyword | description |
| ----- | -- |
| *@parents* | Specifies the connections between schemas. Singular is "to one", plural is "to many" |
