import parse from '~/index'
import fs from 'fs'
import path from 'path'


const fixturesPath = path.join(__dirname, '__fixtures__')
const models = fs.readdirSync(fixturesPath)

models.forEach((modelName: string) => {
  const modelPath = path.join(fixturesPath, modelName)
  const filesForModel = fs.readdirSync(modelPath)

  if (modelName === 'throws') {
    describe('-', () => {
      test(modelName, () => {
        filesForModel.map((filename: string) => {
          const filePath = path.join(modelPath, filename)
          const file = fs.readFileSync(filePath, 'utf8')

          try {
            parse(file, { singular: filename.split('.')[0] })
          } catch (e) {
            expect(e).toBeDefined()
          }
        })
      })
    })
  } else {
    test(modelName, () => {
      filesForModel.map((filename: string) => {
        const filePath = path.join(modelPath, filename)
        const file = fs.readFileSync(filePath, 'utf8')
        expect(parse(file, { singular: filename.split('.')[0] })).toMatchSnapshot(filePath)
      })
    })
  }
})

describe('final test', () => {
  // test('the very first test', () => {
  //   expect(parser()).toEqual('output')
  // })
})
