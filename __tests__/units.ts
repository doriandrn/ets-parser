import parse from '~/index'
import fs from 'fs'
import path from 'path'

const buildOnSchema = fs.readFileSync(path.join(path.resolve(__dirname), '__fixtures__', '_buildOn.ets'), 'utf8')


describe('always valid', () => {
  test('returns a valid object', () => {
    try {
      const x = parse(buildOnSchema)
      console.log(x)
    } catch (e) {
      expect(e).toBeUndefined()
    }
  })
})

describe('I. check & cleanup', () => {
  test('trims empty spaces', () => {

  })
})
